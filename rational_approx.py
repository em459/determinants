import math
import numpy as np
import numpy.polynomial.polynomial as poly
import numpy.polynomial.chebyshev as cheb
import itertools
import scipy, scipy.linalg, scipy.optimize
from matplotlib import pyplot as plt

class RationalApprox(object):
    def __init__(self,n,d,weight,func):
        '''Class for approximating a function :math:`f(x)` defined in the unit
        interval (i.e. :math:`0\le x\le 1`) with a rational function
        of the form :math:`r(x) = p(x)/q(x)` where :math:`p(x)` is a
        polynomial of degree :math:`n` and :math:`q(x)` is a polynomial of
        degree :math:`d`. The error is minimised with respect to the
        provided weight function, i.e. the error is
        :math:`e(x) := (f(x)-r(x))w(x)=(f(x)-p(x)/q(x))w(x)`

        This minimisation is achieved using the modified Remez algorithm
        described in https://arxiv.org/abs/hep-lat/0402037.
        
        :arg n: Degree of numerator polynomial :math:`p(x)`
        :arg d: Degree of denominator polynomial :math:`q(x)`
        :arg weight: Weight function :math:`w(x)`
        :arg func: Function to approximate
        '''
        self.n = n
        self.d = d
        self._weight = weight
        self._func = func
        # Coefficients [p_0,p_1,...,p_n] of polynomial
        # p(x) = p_0 + p_1*x + ... + p_n*x^n
        self._p_coeff = None
        # Coefficients [q_0,q_1,...,q_d] of polynomial
        # q(x) = q_0 + q_1*x + ... + q_d*x^d
        self._q_coeff = None
        # Maximal number of Remez iterations
        self._remez_maxiter = 100
        # Tolerance for Remez algorithm
        self._remez_tolerance = 1.E-4
        # Print convergence information?
        self._verbose = True
        self._remez()

    @property
    def p_coeff(self):
        return self._p_coeff
    
    @property
    def q_coeff(self):
        return self._q_coeff
    
    def __call__(self,x):
        '''Evaluate rational approximation at a particular point

        Given :math:`x`, this returns :math:`r(x)=p(x)/q(x)`

        :arg x: Point to evaluate
        '''
        return poly.polyval(x,self._p_coeff)/poly.polyval(x,self._q_coeff)
        
    def _error(self,x,p_coeff,q_coeff):
        '''Return the value of the (weighted) error at a particular point
        
        Given :math:`x`, and the polynomial coefficients
        [p_0,p_1,...,p_n], [q_0,q_1,...,q_d] of :math:`p(x)` and :math:`q(x)`,
        return :math:`e(x) := (f(x)-r(x))w(x)=(f(x)-p(x)/q(x))w(x)`

        :arg x: Point to evaluate
        :arg p_coeff: Polynomial coefficients [p_0,p_1,...,p_n] of :math:`p(x)`
        :arg q_coeff: Polynomial coefficients [q_0,q_1,...,q_n] of :math:`q(x)`
        '''
        return (self._func(x) - poly.polyval(x,p_coeff)/poly.polyval(x,q_coeff))*self._weight(x)

    def _remez(self):
        '''Calculate the polynomial coefficients [p_0,...,p_n] and
        [q_0,...,q_d] stored in this instance by iterating the two steps of
        the modified Remez algorithm:

        Step 1: given points [x_1,...,x_N], calculate the expansion
                coefficients [p_1,...,p_n], [q_1,...,q_d] such that the error
                at the points :math:`x_i` oscillates between :math:`+\\Delta`
                and :math:`-\\Delta`.
        Step 2: Given the expansion coefficients [p_1,...,p_n] and
                [q_1,...,q_d] calculate the new points [x'_1,...,x'_N] such
                that the error is extremal at those points.
      
        Abort if the relative difference between the error extrema in Step 2
        is below a certain threshold.
        '''
        N = self.n+self.d+2
        x = 1./N*(0.5+np.arange(N))
        c = np.zeros(N+1)
        c[-1] = 1.0
        x = 0.5*(1.0+cheb.chebroots(c))
        if (self._verbose):
            print ('*** Starting Remez algorithm ***')
            print ('iteration   max(Delta)    (max(Delta)-min(Delta))/max(Delta)')
        for i in range(self._remez_maxiter):
            self._p_coeff, self._q_coeff = self._calc_pq_coeff(x)
            x_new = self._update_x(x,self._p_coeff,self._q_coeff)
            Delta = np.array([abs(self._error(z,self._p_coeff,self._q_coeff)) for z in x_new])
            Delta_max = np.amax(Delta)
            Delta_min = np.amin(Delta)
            dDelta = (Delta_max-Delta_min)/Delta_max
            if (self._verbose):
                print (('%9d' % (i+1)), ' ',('%10.4e' % Delta_max), '  ',('%10.4e' % dDelta))
                self._plot_error(x,x_new,self._p_coeff,self._q_coeff,i+1)
            if (dDelta  < self._remez_tolerance):
                print ('*** Remez algorithm converged after '+str(i+1)+' iterations')
                return
            x = x_new
        raise RuntimeError('Remez algorithm failed to converge after '+str(self._remez_maxiter)+' iterations.')
    
    def _null_space(self, A, eps=1e-9):
        '''Calculate the null-space of a matrix.

        This is done by carrying out a SVD of the matrix and collecting all
        vectors which have eigenvalues that are smaller than a specified
        tolerance :math:`\\epsilon`

        :arg eps: tolerance for declaring an eigenvalue to be zero.
        '''
        u, s, vh = scipy.linalg.svd(A)
        null_mask = (s <= eps)        
        null_space = scipy.compress(null_mask, vh, axis=0)
        idx = np.argmin(s)
        null_space = vh[idx]
        return null_space 
    
    def _calc_pq_coeff(self,x):
        '''Step 1 of the Remez algorithm

        Let :math:`N=n+d+2`. Given points :math:`x_1,\\dots,x_N`, construct
        the :math:`N\\tximes N` matrix representing the linear system
        :math:`A(\\Delta)u=0` with :math:`u = (q_0,\\dots,q_d,p_0,\\dots,p_n)`
        defined by
        :math:`\\sum_{j=0}^d q_j x_i^j(f(x_i)-(-1)^i\\Delta/w(x_i))-\\sum_{j=0}^n p_j x_i^j`. To solve this system, find all :math:`\\Delta` such that
        :math:`det(A(\\Delta))=0` and then construct the corresponding vector
        :math:`u` from the nullspace of the matrix. Check :math:`u` for
        validity by verifying that the polynomial :math:`q_0+q_1x+\\dots+q_d^d`
        has no zeros in the unit interval :math:`[0,1]`.

        Return the vectors [p_0,p_1,...,p_n] and [q_0,q_1,...,q_d].

        :arg x: Vector of points
        '''
        # Total number of unknowns
        N = self.n+self.d+2
        # Construct matrices, such that A = mat_0 + Delta*mat_1
        mat_0 = np.zeros((N,N))
        mat_1 = np.zeros((N,N))
        for i in range(N):
            for j in range(self.d+1):
                mat_0[i,j] = x[i]**j*self._func(x[i])
                mat_1[i,j] = x[i]**j*(-1)**(i+1)/self._weight(x[i])
            for j in range(self.n+1):
                mat_0[i,j+self.d+1] = -x[i]**j
        all_coeff = [0,]
        # Calculate determinant of A as a polynomial in Delta
        for p in list(itertools.permutations(range(N))):
            # Work out sign of permutation (currently using O(n^2) algorithm)
            sgn = 1.0
            for j in range(len(p)):
                for i in range(j):
                    sgn *= (p[i]-p[j])/(i-j)
            if (sgn>0):
                sgn = +1
            else:
                sgn = -1
            coeff = [sgn,]
            for i in range(N):
                j = p[i]
                coeff = poly.polymul(coeff,[mat_0[i,j],mat_1[i,j]])
            all_coeff = poly.polyadd(all_coeff,coeff)
        # Clean up by deleting very small coefficients
        all_coeff = [0 if abs(x) < 1.E-14 else x for x in all_coeff]
        pq_coeff = np.zeros(N)
        # Now find all roots Delta of this polynomial and check the
        # corresponding polynomial for roots in q(x)
        allDelta = poly.polyroots(all_coeff)
        for Delta in allDelta:
            mat = mat_0 + Delta*mat_1
            # Check that determinant is zero
            null_space = self._null_space(mat)
#            if (len(null_space) == 0):
#                continue
            pq_coeff = np.array(null_space)
            # Check that this is really a zero of the characteristic polynomial
#            assert np.allclose(poly.polyval(Delta,all_coeff),0.0)
            q_coeff = pq_coeff[0:self.d+1]
            p_coeff = pq_coeff[self.d+1:N]
            if any( (0 <= root) and (root <= 1) for root in poly.polyroots(q_coeff)):
                continue
            # Check that A.pq is close to zero
#            assert np.allclose((np.dot(mat,pq_coeff)),0.0)
            return (p_coeff,q_coeff)
        
    def _update_x(self,x,p_coeff,q_coeff):
        ''' Step 2 of the Remez algorithm.

        Given polynomial expansion coefficients [p_0,...,p_n] of :math:`p(x)`
        and [q_0,...,q_d] of :math:`q(x)` and :math:`N=n+d+2` points
        :math:`x_1,\\dots,x_N`, find a new set of points
        :math:`x'_1,\\dots,x'_d` such that the weighted error :math:`e(x)` is 
        extremal at those points. For this, find the :math:`N-1` zeros
        :math:`z_1,\\dots,z_{N-1}` of :math:`e(x)` and divide :math:`[0,1]`
        into :math:`N` subintervals :math:`I_i = [a_i,b_i]` with
        :math:`a_i=z_{i-1}`, where :math:`b_i=z_i` (with boundary cases
        :math:`a_1 = 0`, :math:`b_{N}=1`). Then we have
        :math:`x'_i = argmax_{z\in I_i} e(z)`

        Return array with new set of points :math:`x'_1,\\dots,x'_N`

        :arg x: Current set of points :math:`x_1,\\dots,x_N`
        :arg p_coeff: Current expansion coefficients [p_0,...,p_n] of :math:`p(x)`
        :arg q_coeff: Current expansion coefficients [q_0,...,q_n] of :math:`q(x)`
        '''
        N = len(x)
        f_error = np.vectorize(lambda z: self._error(z,p_coeff,q_coeff))
        # Construct intervals I_i
        intervals = [0.0]
        for i in range(N-1):
            a,b = x[i],x[i+1]
            root = scipy.optimize.bisect(f_error,a,b)
            intervals.append(root)
        intervals.append(1.0)
        x_new = []
        # Find local extrema
        for i in range(N):
            if f_error(x[i]) < 0.0:
                f = f_error
            else:
                f = lambda x: -f_error(x)
            xopt = scipy.optimize.fminbound(f,intervals[i],intervals[i+1])
            x_new.append(xopt)
        return x_new

    def _plot_error(self,x,x_new,p_coeff,q_coeff,i):
        '''Plot the error for debugging and save as file 'error_i.pdf'.
        
        :arg x: Current points :math:`x_i` used as input for Step 1 of the
                Remez algorithm
        :arg x_new: New points after Step 2 of the Remez algorithm
        :arg p_coeff: Coefficients [p_0,...,p_n] obtained in Step 1 of the
                Remez algorithm
        :arg q_coeff: Coefficients [q_0,...,q_d] obtained in Step 1 of the
                Remez algorithm
        :arg i: index out output file
        '''
        f_error = np.vectorize(lambda z: self._error(z, p_coeff,q_coeff))
        plt.clf()
        z = np.arange(0,1,0.0001)
        ax=plt.gca()
        ax.set_xlim(0,1)
        Delta = f_error(x[0])
        ax.set_ylim(-10*abs(Delta),10*abs(Delta))
        plt.plot([0,1],[0,0],linewidth=1,
                 color='black',linestyle='-')
        plt.plot([0,1],[Delta,Delta],linewidth=2,
                 color='black',linestyle='--')
        plt.plot([0,1],[-Delta,-Delta],linewidth=2,
                 color='black',linestyle='--')
        plt.plot(z,f_error(z),
                 linewidth=2,color='blue')
        plt.plot(x,f_error(x),
                 linewidth=0,
                 markersize=4,
                 marker='o',
                 color='red')
        plt.plot(x_new,f_error(x_new),
                 linewidth=0,
                 markersize=4,
                 marker='s',
                 color='green')
        plt.savefig('error_'+('%04d' % i)+'.pdf',bbox_inches='tight')
