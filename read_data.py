import numpy as np
from matplotlib import pyplot as plt

def read_matrix(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
        n = len(lines)
        A = np.zeros((n,n))
        for i,line in enumerate(lines):
            s = line.split()
            for j in range(n):
                A[i,j] = float(s[j])
    return A

A_XTWX = read_matrix("OFI.txt")
A_S = read_matrix("P.txt")

A = A_XTWX + A_S

w, v = np.linalg.eig(A)
y = np.zeros(len(w))

plt.clf()
ax = plt.gca()
ax.set_xscale('log')
plt.plot(w,y,marker='o',markersize=4,linewidth=0)
plt.savefig('eigenvalues.pdf',bbox_inches='tight')
