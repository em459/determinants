from rational_approx import *
from partial_fraction import *
import numpy as np

# Polynomial degree of numerator
n = 2
# Polynomial degree of denominator
d = 3

# Function to approximate
f = lambda x: np.log(0.01+x)

approx = RationalApprox(n,d,lambda x: 1, f)

part_frac = PartialFraction(approx.p_coeff,approx.q_coeff)

print ('positions of poles = ',part_frac.x_pole)
print ('stength of poles   = ',part_frac.A_pole)

x = np.arange(0,1,0.0001)

plt.clf()
p_f = plt.plot(x,np.vectorize(f)(x),linewidth=2,color='blue')[0]
p_approx = plt.plot(x,np.vectorize(approx)(x),linewidth=2,color='red')[0]
p_part_frac = plt.plot(x,np.vectorize(part_frac)(x),linewidth=2,color='green')[0]
ax = plt.gca()
ax.set_xlim(0,1)
ax.set_xlabel('$x$')
plt.legend((p_f,p_approx,p_part_frac),('$f(x)$','$r(x)$','part frac of $r(x)$'))
plt.title(' n = '+str(n)+' d = '+str(d))
plt.savefig('function.pdf',bbox_inches='tight')
