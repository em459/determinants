import numpy as np
import numpy.polynomial.polynomial as poly

class PartialFraction(object):
    def __init__(self,p_coeff,q_coeff):
        '''Given a rational polynomial of the form :math:`r(x)=p(x)/q(x)`
        with :math:`p(x)=p_0+p_1x+\\dots+p_{n-1}x^{n-1}` and
        :math:`q(x)=q_0+q_1x+\\dots+q_nx^n`, construct the sum of
        single poles :math:`r(x) = \\sum_{j=0}^{n-1} A_j/(x-x_j)`

        :arg p_coeff: Coefficient [p_0,p_1,...,p_{n-1}] of :math:`p(x)`
        :arg q_coeff: Coefficient [q_0,q_1,...,q_n] of :math:`q(x)`
        '''
        self._n = len(q_coeff)-1
        assert(len(p_coeff)-1 == self._n-1)
        self._p_coeff = p_coeff
        self._q_coeff = q_coeff
        self._construct()

    def __call__(self,x):
        '''Evaluate the single-pole sum at point :math:`x`

        :arg x: Point :math:`x` at which to evaluate
        '''
        s = 0.0
        for j in range(self._n):
            s += self._Apole[j]/(x-self._xpole[j])
        return s

    @property
    def x_pole(self):
        '''Return the position on poles [x_0,...,x_n]'''
        return self._xpole

    @property
    def A_pole(self):
        '''Return the strength of the poles [A_0,...,A_n]'''
        return self._Apole
    
    def _construct(self):
        '''Construct :math:`x_0,\\dots,x_n` and :math:`A_0,\\dots,A_n`

        The zeros :math:`x_j` are simply the roots of :math:q(x):. Given those,
        the poles :math:`A_j` are given as
        :math:`A_j = C p(x_j)/((x_j-x_0)\\dots(x_j-x_{j-1}))(x_j-x_{j+1})\\dots(x_j-x_n)` where :math:`N` is a normalisation factor.
        '''
        self._xpole = poly.polyroots(self._q_coeff)
        nrm = 1./poly.polyval(0.5,self._q_coeff)
        for j in range(self._n):
            nrm *= (0.5-self._xpole[j])
        self._Apole = np.ndarray(self._n)
        for j in range(self._n):
            self._Apole[j] = nrm*poly.polyval(self._xpole[j],self._p_coeff)
            for k in range(self._n):
                if (not (k==j)):
                    self._Apole[j] *= 1./(self._xpole[j]-self._xpole[k])
